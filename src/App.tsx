import '@fortawesome/fontawesome-free/css/all.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from 'react';
import './assets/theme/styles.css';
import Experiences from './components/experiences';
import Knowledge from './components/knowledge';
import Projects from './components/projects';
import Qualifications from './components/qualifications';
import References from './components/references';
import Sidebar from './components/sidebar';
import SoftSkills from './components/softskills';
import Tags from './components/tools';
import AboutMe from './components/aboutMe';
import ProfessionalSkills from './components/professionalskills';

interface Props {
  aboutMe: any,
  profile: any,
  experiences: any,
  projects: any,
  knowledges: any,
  tools: any,
  qualifications: any,
  softskills: any,
  professionalSkills: any
}

const CV: React.FunctionComponent<Props> = (props) => {

  const { aboutMe, profile, experiences, projects, tools, knowledges, qualifications, softskills, professionalSkills } = props;

  const renderAboutMe = () => {
    if (aboutMe) {
      return(<AboutMe {...aboutMe} />);
    } else {
      return null;
    }
  }

  const renderExperiencesSection = () => {
    if (experiences) {
      return (<Experiences {...experiences} />);
    }
    return null;
  }

  const renderProjectsSection = () => {
    if (projects) {
      return (<Projects {...projects} />);
    }
    return null;
  }

  const renderTags = () => {
    if (tools) {
      return (<Tags {...tools} />);
    }
    return null;
  }

  const renderTechnicalSkills = () => {
    if (knowledges) {
      return (<Knowledge {...knowledges} />);
    }
    return null;
  }

  const renderQualifications = () => {
    if (qualifications) {
      return (<Qualifications {...qualifications} />);
    }
    return null;
  }

  const renderProfessionalSkills = () => {
    if(professionalSkills) {
      return (<ProfessionalSkills {...professionalSkills} />);
    }
    return null;
  }

  const renderSoftSkills = () => {
    if (softskills) {
      return (<SoftSkills {...softskills} />);
    }
    return null;
  }

  const renderReferences = () => {
    return (<References />);
  }

  return (
    <div className="wrapper">
      <Sidebar
        {...profile}
      />
      <div className="main-wrapper">
        {renderAboutMe()}
        {renderExperiencesSection()}
        {renderQualifications()} 
        {renderProfessionalSkills()}
      </div>
      <div className='main-wrapper full-width-wrapper'>
      {renderTechnicalSkills()}
        {renderSoftSkills()}
        {renderTags()}
        {renderReferences()}   
      </div>
      <div className="main-wrapper project-wrapper">
        {renderProjectsSection()}
      </div>
    </div>
  );
}


export default CV;