import * as React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';
import Data from './data';


const container = document.getElementById('root');
const root = createRoot(container!);
root.render(
    <React.StrictMode>
        <App {...Data} />
    </React.StrictMode>
    
);