export const projects = {
  sectionTitle: 'Projects',
  icon: 'fas fa-clipboard-list',
  // description: 'Projects, I have completed & attended are listed below.',
  categories: [
    {
      name: 'Confinale',
      city: 'Zurich',
      list: [            
        {
          url: 'https://www.confinale.com/',
          title: 'Reclaimer',
          description: 'I am working on our product "Reclaimer". My goal is to update dependencies, move tests to JUnit 5 and upgrade code to run on JDK 17. I also organized a tech talk where I shared my knowledge on relevant JDK 17 features.',   
          dateFrom: '2022-10-30',
          dateTo: 'now',
          role: 'Fullstack Developer',
          technologies: [
            'Kotlin', 'Java', 'gradle', 'Docker', 'MSSQL', 'JUnit 5', 'Dropwizard'
          ]
        }
      ],
    },
    {
      name: 'Avaloq',
      city: 'Zurich',
      list: [
        {
          url: 'https://www.avaloq.com/',
          title: 'Community API / CAPI',
          description: 'Community API is a collection of REST APIs and helps to integrate avaloq data to new applications. As a team member, I was working on feature improvements and writing missing tests. My daily activity is to help clients solve their issues and to review code, based on business value and code quality. I am responsible for the documentation of CAPI for our clients and developers. I gave a training to  our team about writing the documentation in adoc.',   
          dateFrom: '2020-07-01',
          dateTo: '2022-09-01',
          role: 'Fullstack Developer',
          technologies: [
            'Java', 'Spring Boot', 'JUnit', 'Oracle DB', 'Jenkins', 'gradle', 'Kafka', 'Liquibase', 'Swagger', 'adoc', 'Postman'
          ]
        }
      ]
    },
    {
      name: 'Confinale',
      city: 'Zurich',
      list: [      
        {
          url: 'https://www.confinale.com/',
          title: 'Team events',
          description: 'In a small team we are organizing social activities and games (e.g. "Sonntagsmaler"). Furthermore we developed a website called "videowall", where our collegues provide a short video to introduce themselves and share a fun fact with us. I implemented the frontend part of videowall using the Svelte compiler.',   
          dateFrom: '2020-05-01',
          dateTo: '2020-06-01',
          role: 'Fullstack Developer',
          technologies: [
            'Svelte', 'Docker'
          ]
        },
        {
          url: 'https://www.confinale.com/',
          title: 'Reclaimer',
          description: 'I am working on our product "Reclaimer", which helps banks and service providers to efficiently manage withholding tax reclaims. I worked on product improvements and new features, presenting solutions regurlarly and support our clients. My daily activities also include code reviews. I achieved two major milestones: <br/>* Reclaimer targets JDK 11<br/>* Reclaimer runs on Openshift. The goal is to easily reproduce states of "Reclaimer" with our clients configuration and efficiently solve client issues.',   
          dateFrom: '2019-08-01',
          dateTo: '2020-06-30',
          role: 'Fullstack Developer',
          technologies: [
            'Kotlin', 'Java', 'TypeScript', 'Angular', 'Dropwizard', 'Swagger', 'Liquibase', 'SQL', 'gradle', 'Docker', 'MSSQL', 'Camunda'
          ]
        }
      ],
    },
    {
      name: 'ti&m AG',
      city: 'Zurich',
      list: [
        {
          url: 'https://www.ti8m.com/',
          title: 'ti&m Channel Suite',
          description: 'In a large team, we are developing new features and customize ti&m channel suite for the customers needs. I developed a library for common validation tasks using Bean Validation. Furthermore I refactored modules to be more configurable and speed up deployment time.',
          dateFrom: '2019-04-01',
          dateTo: '2019-07-30',
          role: 'Fullstack Developer',
          technologies: [
            'Kotlin', 'Java', 'TypeScript', 'Spring Boot', 'Liquibase', 'SQL', 'gradle', 'Docker' 
          ]
        }
      ],
    },
    {
      name: 'SIX Payment Services | Worldline',
      shouldPageBreak: true,
      city: 'Zurich & Biel/Bienne',
      list: [
        {
          url: 'https://www.six-payment-services.com/',
          title: 'Merchant Onboarding',
          description: 'I am developing the registration process for merchant oboarding . My responsibility is to adapt requirements and regulations (GDPR) to the frontend, backend and database.',
          dateFrom: '2018-02-01',
          dateTo: '2019-03-30',
          role: 'Fullstack Developer',
          technologies: [
            'TypeScript', 'webpack', 'yarn', 'AngularJS 1.4.x', 'Bootsrap', 'Jasmine', 'Jest', 'Spring Boot', 'Oracle 11g', 'MySQL', 'Flyway', 'Wildfly', 'Docker', 'Maven'
          ]
        },
        {
          url: 'https://www.six-payment-services.com/',
          title: 'Merchant Portal',
          description: 'SIX is developing a new application for merchants to get granular statistics on payment transactions. I am designing and implementing the REST services and the frontend components using Spring Boot and ReactJS .',
          dateFrom: '2018-01-01',
          dateTo: '2019-03-30',
          role: 'Fullstack Developer',
          technologies: ['TypeScript', 'webpack','yarn', 'ReactJS 16.x', 'Bootsrap', 'Jasmine', 'Jest', 'Java Spring', 'Oracle 11g', ' Wildfly', 'Docker', 'Maven']
        },
        {
          url: 'https://www.six-payment-services.com/',
          title: 'JavaScript Terminal Library',
          description: 'By developing a JavaScript library, payment solutions (based on the SixML protocol) can be realized with modern browsers. The prototype shows that even tablet browsers can be used as cash registers. The communication is based on WebSockets,so the library supports bidirectional coomunication between the web app and the terminal.',
          dateFrom: '2017-01-07',
          dateTo: '2018-01-30',
          role: 'Fullstack Developer',
          technologies: ['TypeScript', 'JavaScript', 'webpack', 'node', 'npm', 'WebSockets', 'CucumberJS', 'Jasmine']
        },
      ],
    },
    {
      name: 'SBB',
      city: 'Bern & Olten',
      list: [
        {
          url: 'https://sbb.ch',
          title: 'Rail Maintenance Management on iPad',
          description: 'With an iPad App, mechanics can record, document and process maintenance work faster and more reliable. This enables the business to identify problems more quickly, create financial plans and react immediately.',
          dateFrom:'2016-09-01',
          dateTo: '2017-08-30',
          role: 'Senior Mobile Engineer',
          technologies: ['iOS', 'Swift', 'Objective-C', 'CocoaPods', 'fastlane', 'OCMock', 'AFNetworking', 'PSPDFKit', 'Core Data']
        },
        {
          url: 'https://sbb.ch',
          title: 'SwissPass Single Sign On using OAuth for Mobile Apps',
          description: 'SBB wants to simplify logins on all their web and mobile apps. The idea is to use the SwissPass Login as the account provider. The login is done in the browser. After a successfull login, the browser redirects back to the app (where the onboarding procedure started) and also sends the access and refresh token. The library refreshes the access token if needed, so app makers can focus on their key features instead of handling user logins.',
          dateFrom:'2016-02-01',
          dateTo:'2016-08-30',
          role: 'Senior Mobile Engineer',
          technologies: ['iOS', 'Swift', 'Objective-C', 'Safari Browser','CocoaPods', 'OAut2', 'fastlane']
        },
      ]
    },
    {
      name: 'CA | Crédit Agricole SA',
      city: 'Lausanne & Geneva',
      list: [
        {
          url: 'https://www.credit-agricole.fr/',
          title: 'Linxo Integration in Credit Agricole App',
          description: 'We integrated the Linxo Framework, so users are able connect third party bank accounts from europe. We designed the onboarding process, defined the REST services for the backend and integrated  the new features to the iOS and Android App',
          dateFrom:'2017-01-01',
          dateTo: '2017-09-30',
          role: 'Senior Mobile Engineer',
          technologies: ['iOS', 'Objective-C', 'CocoaPods', 'fastlane']
        },
      ]
    },
    // {
    //   name: 'BCV | Banque Cantonale Vaudoise',
    //   shouldPageBreak: true,
    //   city: 'Lausanne',
    //   list: [
    //     {
    //       url: 'https://www.bcv.ch/',
    //       title: '2-Factor-Authentication on Mobile',
    //       description: 'The integration of strong authentication leads to more security in BCVs mobile apps. We used the ti&m Security Suite and Gemalto Mobile Authentication Suite to implement a token-based authentification. Using the token the app is allowed to request data of the user. The token can be revoked at any time on the portal website, in case the token or the phone was stolen.',
    //       dateFrom:'2015-07-01',
    //       dateTo: '2016-01-30',
    //       role: 'Mobile Engineer',
    //       technologies: ['iOS', 'Objective-C', 'Gemalto SDK', '2FA', 'OAuth2', 'CocoaPods', 'fastlane']
    //     },
    //   ]
    // }
  ]
};

export default projects;
