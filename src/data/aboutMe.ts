export const aboutMe = {
  icon: 'fas fa-user',
  motto: 'Data shows us the source and impact of problems. I love to use technology and build the best possible solutions to solve them!',
  description: 'During my studies I came to Switzerland in 2011 and started my career as a Software Engineer. \nOver the years I developed mobile applications, web frontend apps and backend services in the cloud. \nIn the past few years I mainly worked on Spring Boot based applications running in OpenShift.',
  sectionTitle: 'About Me'
};

export default aboutMe;
