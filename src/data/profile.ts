const profile = {
  imagePath: 'profile.png',
  name: 'Michael Wolski',
  currentRole: 'Fullstack Developer',
  currentCompany: 'Confinale AG',
  personalDetails: {
    sectionTitle: 'Personal',
    birthday: '1989-05-04T02:22:00Z',
    nationality: 'German',
    city: '8105 Regensdorf-Watt',
    address: 'Roosstrasse 44B',
    country: 'Switzerland',
  },
  socialMedia: {
    sectionTitle: 'Social Media',
    website: null,
    linkedin: 'linkedin.com/in/miwolski',
    github: 'github.com/mwolski89',
    twitter: null,
    // stackoverflow: 'stackoverflow.com/users/3310724/chuck',  
  },
  contactDetails: {
    sectionTitle: 'Contact',
    mail: 'michaelwolski2010@gmail.com',
    phoneNumber: '+41 78 617 98 74',
  },
  educationDetails: {
    sectionTitle: 'Education',
    list: [
      {
        name: 'BSc in Business Informatics',
        school: 'TH Mittelhessen, Germany',
        degree: 'BSc in Business Informatics',
        date: '2008-2011' },
    ]
  },
  languages: {
    sectionTitle: 'Languages',
    list: [{ name: 'German', level: 'Native' }, { name: 'English', level: 'Professional' }, { name: 'Polish', level: 'Native' }]
  },
  interests: {
    sectionTitle: 'Interests',
    list: [
      'Reading',
      'Photography',
      'Culture',
      'Smart devices',
      'Sport activities',
      'Movies',
      'Computer games',
    ]
  }
};

export default profile;
