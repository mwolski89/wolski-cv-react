export const qualifications = {
    sectionTitle: 'Qualifications',
    icon: 'fas fa-award',
    list: [
        'REST with Spring - Certificate of completion, Baeldung',
        'Spring Data JPA - Certificate of completion, Baeldung',
        'Spring Security Core - Certificate of completion, Baeldung',
    ]
}

export default qualifications;
