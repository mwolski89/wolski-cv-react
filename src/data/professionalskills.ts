export const qualifications = {
    sectionTitle: 'Professional skills',
    icon: 'fa-solid fa-user-gear',
    list: [
        'Agile project management with Scrum & Kanban.',
        'DevOps protagonist.',
        'Test driven development.',
        'Expert know-how in mobile & web development and user experience.',
    ]
}

export default qualifications;
