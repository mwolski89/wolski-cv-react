export const experiences = {
  sectionTitle: 'Work Experiences',
  icon: 'fa fa-briefcase',
  list: [
    {
      title: 'Fullstack Developer',
      company: 'Confinale AG',
      description: 'Confinale is a specialist for tax, compliance, legal reporting, asset management and cash allocation. I work on our product Reclaimer, build applications for Avaloq and support their clients.',
      companyLink: 'https://confinale.ch',
      companyShortDetail: '',
      dateFrom: '2019-08-01',
      dateTo: 'now'
    },
    {
      title: 'Senior Software Engineer',
      company: 'ti&m AG',
      description: 'ti&m creates solutions and products for customer needs. My responsibilities were creating technical concepts, designing UX workflows and implementing the best possible solutions. I worked on mobile and web projects for SBB, Credit Agricole, SIX Payment Solution and Wordline. I supported in establishing of a new office in Lausanne.',
      companyLink: 'https://ti8m.com',
      companyShortDetail: '',
      dateFrom: '2018-01-01',
      dateTo: '2019-07-31'
    },
    {
      title: 'Software Engineer',
      company: 'ti&m AG',
      description: 'I started my career as a mobile developer and worked on several prototypes for public transportation, healthcare and financial mobile apps.',
      companyLink: 'https://ti8m.com',
      companyShortDetail: '',
      dateFrom: '2011-06-01',
      dateTo: '2017-12-31',
    },
    {
      title: 'Internship | Software Engineer',
      company: 'Business Solution Group Technology Innovation AG',
      description: 'In coorperation with BSG TI, I finished my bachelor thesis "Mobile CMS for Mobile Content Delivery"',
      companyLink: 'https://www.bsgroup.ch/',
      companyShortDetail: '',
      dateFrom: '2011-02-01',
      dateTo: '2011-05-31'
    }
  ]
};

export default experiences;
