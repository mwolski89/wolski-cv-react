export const knowledges = {
    sectionTitle: 'Technical Skills',
    icon: 'fas fa-code',
    list: [
      {name: 'Backend, Java', level: 100},
      {name: 'Web, JavaScript, TypeScript', level: 75},
      {name: 'iOS, Objective-C, Swift', level: 75},
      {name: 'Android, Java, Kotlin', level: 60},
      {name: 'Command line tools Win/Unix', level: 30},
      {name: 'Python', level: 30}
    ]
  };

  export default knowledges;
