export const softskills = {
    sectionTitle: 'Soft Skills',
    icon: 'fa-solid fa-person-rays',
    list: [
      'Team player',
      'Open minded',
      'Initiator',
      'Mentor',
      'Listener and communicator',
      'Agile',
      'Analytical thinking',
      'KISS principle',
      'Creative',
      'Goal oriented',
    ]
  };

  export default softskills;
