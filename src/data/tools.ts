export const tools = {
  sectionTitle: 'Tools & Technologies',
  icon: 'fas fa-tools',
  list: [
    'IntelliJ',
    'VS Code',
    'maven',
    'gradle',
    'Jenkins',
    'git',
    'Jira',
    'webpack',
    'npm/Yarn',
    'Node',
    'ReactJS',
    'Jasmine',
    'Jest',
    'Docker',
    'Spring Boot',
    'Python scripting',
    'MySQL',
    'Unix',
    'Fedora',
    'Windows',
    'macOS', 
    'iOS', 
    'Android',
  ]
};

export default tools;
