import aboutMe from './aboutMe';
import experiences from './experiences';
import knowledges from './knowledges';
import profile from './profile';
import projects from './projects';
import qualifications from './qualifications';
import softskills from './softskills';
import tools from './tools';
import professionalSkills from './professionalskills';

export const data = {
  profile,
  aboutMe,
  experiences,
  projects,
  knowledges,
  tools,
  qualifications,
  softskills,
  professionalSkills
};

export default data;
