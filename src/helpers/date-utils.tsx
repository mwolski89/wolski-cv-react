
/**
 * Calculate the Age in years. 
 * @param {*} dateString A string of the birthday in the UTC format, e.g. 1989-05-04T02:22:00Z
 */
export function getAge(dateString: string): number {
    const today = new Date();
    const birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    const month = today.getMonth() - birthDate.getMonth();
    if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
}

/**
 * formats the birthday in the specific locale.
 * @param {*} dayOfBirth A string of the birthday in the UTC format, e.g. 1989-05-04T02:22:00Z
 */
export function formatBirthday(dateString: string): string {
    const lang =  getNavigatorLanguage();
    const options: Intl.DateTimeFormatOptions = { day:'numeric', month: 'short', year: 'numeric'};

    return new Date(dateString).toLocaleString(lang, options);
}

export function formatDateToString(dateString: string): string {
    if (dateString === 'now') {
        return dateString;
    }
    
    const date = new Date(dateString);
    const options: Intl.DateTimeFormatOptions = { month: 'short', year: 'numeric'};
    const lang = getNavigatorLanguage();
    return date.toLocaleString(lang, options);
}

const getNavigatorLanguage = () => 'en';
    // (navigator.languages && navigator.languages.length) ? navigator.languages[0] : navigator.userLanguage || navigator.language || navigator.browserLanguage || 'en';

