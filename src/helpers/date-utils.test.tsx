import { formatBirthday, getAge, formatDateToString } from './date-utils';


it('should format birthday', () => {
    // GIVEN
    const birthDate = "1989-05-04T02:22:00Z";
    
    // WHEN
    const result = formatBirthday(birthDate);

    // THEN
    expect(result).toBe("May 4, 1989")
});

it('should calculate age by dd.MM.yyyy', () => {
    // GIVEN
    const birthDate = "04.05.1989";

    // WHEN
    const result = getAge(birthDate);

    // THEN
    expect(result).toBe(33);
})

it('should calculate age by datetime', () => {
    // GIVEN
    const birthDate = "1989-05-04T02:22:00Z";

    // WHEN
    const result = getAge(birthDate);

    // THEN
    expect(result).toBe(33);
})

it('should format a date to MMM yyyy', () => {
    // GIVEN
    const dateString = "2020-07-01";

    // WHEN
    const result = formatDateToString(dateString);

    // THEN
    expect(result).toBe("Jul 2020");
})

it('should format a date to "now"', () => {
    // GIVEN
    const dateString = "now";

    // WHEN
    const result = formatDateToString(dateString);

    // THEN
    expect(result).toBe("now");
})
