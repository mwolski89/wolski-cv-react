import * as React from 'react';
import { render } from '@testing-library/react';

import App from './App';
import Data from './data';

it('renders without crashing', () => {
  render(
    <App {...Data} />
  );
});
