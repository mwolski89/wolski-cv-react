
import * as React from 'react';
import Section from './shared/section';


const References: React.FunctionComponent<{}> = () => {
    return (
        <Section
        className="references-section"
        icon="fas fa-users"
        id="references"
        title="References"
    >
        <div className="references">
            <span>On request I will be happy to give you the contact to my references (colleagues and customers).</span>
        </div>
    </Section>
    )
};

export default References;