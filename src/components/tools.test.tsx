import * as React from 'react';
import { screen } from '@testing-library/react';
import { createRoot } from 'react-dom/client';
import { act } from 'react-dom/test-utils';

import Tools from './tools';

let container: Element | null = null;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
    if (container !== null) {
        document.body.removeChild(container);
        container = null;
    }
});

describe("Tools Test Suite", () => {      
    it("Should render tools section", () => {  
        // GIVEN
        const sectionTitle = 'tools test';
        const icon =  '';
        const list = [
            'Xcode', 
            'IntelliJ',
            'VS Code',
            'R Studio',
            'maven'
        ];

        // WHEN
        act(() => {
            createRoot(container!)
                .render(
                    <Tools 
                        sectionTitle={sectionTitle}
                        list={list}
                        icon={icon}
                    />
            );
        });
        
        // THEN
        expect(screen.getByText(sectionTitle)).toBeInTheDocument;

        list.map((tool) => {
            expect(screen.getByText(tool)).toBeInTheDocument();
        });
    });
});
