
import * as React from 'react';
import Section from './shared/section';


interface Props {
  list: [];
  sectionTitle: string;
  icon: string;
}
const Qualifications: React.FunctionComponent<Props> = (props: Props) => {
  const { icon, sectionTitle, list} = props;

  const renderLine = (content: string) => {
      return (
          <li key={content}>{content}</li>
      );
  }
        
  return (
    <Section
        className="qualifications-section"
        icon={icon}
        id="qualifications"
        title={sectionTitle}
    >
        <div className="qualifications">
            <ul>
            {
                list.map((content) => {
                    return renderLine(content);
                })
            }
            </ul>
        </div>
    </Section>
  );      
}

export default Qualifications;