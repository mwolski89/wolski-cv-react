import * as React from 'react';
import { formatDateToString } from './../helpers/date-utils';
import Section from './shared/section';


interface Props {
  list: [],
  sectionTitle: string,
  icon: string
}
const Experiences: React.FunctionComponent<Props> =(props: Props) => {

  const { icon, sectionTitle, list } = props;


  const renderListItem = (job:any, i: number) => {
    const timeAtJob = `${formatDateToString(job.dateFrom)} - ${formatDateToString(job.dateTo)}`;
    return (
      <div className="item"  key={`exp_item_${i}`}>
        <div className="meta">
          <div className="upper-row">
            <h3 className="job-title">{job.title}</h3>
          </div>
          {renderCompanySection(job.company, job.companyLink, job.companyShortDetail, timeAtJob)}
        </div>
        <div className="details">
          <p dangerouslySetInnerHTML={{ __html: job.description }} />
        </div>
      </div>
    );
  }
  const renderCompanySection = (company: string, companyLink: string, companyShortDetail: string, timeAtJob: string) => {
    if (company && companyLink) {
      return (
        <div className="company"> 
          <a href={companyLink} target="_blank">{company}</a>
          <div>{timeAtJob}</div>
          {companyShortDetail || ''}
        </div>
      );
    }
    return null;
  }

    return (
      <Section
        className="experieces-section"
        icon={icon}
        title={sectionTitle}
        id="experiences"
      >
        {list.map((item, i) => {
          return renderListItem(item, i);
        })}
      </Section>
    );
  }

export default Experiences;