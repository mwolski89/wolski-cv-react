
import * as React from 'react';
import Section from './shared/section';


interface Props {
  list: [];
  sectionTitle: string;
  icon: string;
}
const ProfessionalSkills: React.FunctionComponent<Props> = (props: Props) => {
  const { icon, sectionTitle, list} = props;

  const renderLine = (content: string) => {
      return (
          <li key={content}>{content}</li>
      );
  }
        
  return (
    <Section
        className="professionalSkills-section"
        icon={icon}
        id="professionalSkills"
        title={sectionTitle}
    >
        <div className="professionalSkills">
            <ul>
            {
                list.map((content) => {
                    return renderLine(content);
                })
            }
            </ul>
        </div>
    </Section>
  );      
}

export default ProfessionalSkills;