
import * as React from 'react';
import Section from './shared/section';


interface Props {
    list: any[];
    sectionTitle: string;
    icon: string;
}

const SoftSkills: React.FunctionComponent<Props> = (props: Props) => {

    const { sectionTitle, icon, list } = props;
    

    const renderLine = (content: string) => {
        return (
            <li key={content}>{content}</li>
        );
    }

    return (
      <Section
          className="softskills-section"
          icon={icon}
          id="softskills"
          title={sectionTitle}
      >
          <div className="softskills">
              <ul className="skillset">
              {
                  list.map((content: string) => {
                      return renderLine(content);
                  })
              }
              </ul>
          </div>
      </Section>
    );      
}

export default SoftSkills;