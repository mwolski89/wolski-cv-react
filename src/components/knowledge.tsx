import * as React from 'react';
import Section from './shared/section';
import SkillBox from './skill-box/skill-box';


interface Props {
  list:[];
  icon: any;
  sectionTitle: string
}

const Knowledge: React.FunctionComponent<Props> = (props: Props) => {

  const { list, icon, sectionTitle } = props;
  
  return (
    <Section
      className="knowledge-section"
      icon={icon}
      id="knowledge"
      title={sectionTitle}
    >
      <div className="knowledge">
        {
          list.map((knowledge: any) => {
            return <SkillBox skill={knowledge} key={knowledge.name}/>
          })
        }
      </div>
    </Section>
  );
}


export default Knowledge;