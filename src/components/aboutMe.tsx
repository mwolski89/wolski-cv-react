import * as React from 'react';
import Section from './shared/section';


interface Props {
  sectionTitle: string,
  icon: string,
  description: string
}
const AboutMe: React.FunctionComponent<Props> =(props: Props) => {

  const { icon, sectionTitle, description } = props;
 
  return (
    <Section
        className="about-me-section"
        icon={icon}
        id="about-me"
        title={sectionTitle}
    >
     {description}   
    </Section>
  );  
}    

export default AboutMe;