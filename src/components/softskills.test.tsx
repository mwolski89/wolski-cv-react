import * as React from 'react';
import { render, screen } from '@testing-library/react';

import SoftSkills from './softskills';


describe("Softskills Test Suite", () => {
    it("Should render softskills section", () => {

        // GIVEN
        const sectionTitle = 'softskills test';
        const icon =  '';
        const list = [
            'Team player',
            'Open minded',
            'Initiator'
        ];
    
        
        // WHEN
        render(
            <SoftSkills 
                sectionTitle={sectionTitle}
                list={list}
                icon={icon}
            />
        );
    
        // THEN
        expect(screen.getByText(sectionTitle)).toBeInTheDocument()
        list.map((tool) => {
            expect(screen.getByText(tool)).toBeInTheDocument();
        });
    });
})
