import * as React from 'react';
import Section from './shared/section';


interface Props {
  list: any[];
  sectionTitle: string;
  icon: string;
}

const Tools: React.FunctionComponent<Props> = (props: Props) => {
  const { icon, sectionTitle, list } = props;
    
  const renderListItem = (item: any, i: number) => {
    return (
      <li key={`tool_item_${i}`}>
        {item}
      </li>
    );
  }
  
  return (
    <Section
      className="tools-section"
      icon={icon}
      id="tools"
      title={sectionTitle}
    >
      <div className="tools">
        <ul className="skillset">
          {list.map((item, i) => {
            return renderListItem(item, i);
          })}
        </ul>
      </div>
    </Section>
  );
  
}

export default Tools;
