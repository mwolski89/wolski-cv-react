import * as React from 'react';

interface Props {
  title: string;
  website: string;
  linkedin: string;
  github: string;
  stackoverflow: string;
}

const SocialMedia: React.FunctionComponent<Props> = (props: Props) => {

  const { title, linkedin, github, stackoverflow } = props;

  const renderListItemWithIcon = (className: string, data: string, iconName: string, type: string) => {
    if (!data) { 
      return null; 
    }

    let href = data;
    switch (type) {
      case 'email':
        href = `mailto: ${data}`;
        break;
      case 'phone':
        href = `tel:${data}`;
        break;
    }
    
    if (type !== undefined) {
      return (
        <li className={className}>
          <i className={`${iconName}`} />
          <a href={`//${href}`} target="_blank"> {data} </a>
        </li>
      );
    } else {
      return (
        <li className={className}>
          <i className={`${iconName}`} />
          <a> {data} </a>
        </li>
      )
    }
  }
   
  return (
    <div className="personal-container container-block">    
      <h2 className="container-block-title">{title}</h2>
      <ul className="list-unstyled personal-list">
        {renderListItemWithIcon('linkedin', linkedin, 'fab fa-linkedin', 'link')}
        {renderListItemWithIcon('github', github, 'fab fa-github', 'link')}
        {renderListItemWithIcon('stackoverflow', stackoverflow, 'fab fa-stack-overflow', 'link')}
      </ul>
    </div>
  );

}

export default SocialMedia;
