import * as React from 'react';
import { formatBirthday, getAge } from './../../helpers/date-utils';

interface Props {
  title: string;
  website: string;
  birthday: string;
  nationality: string;
  city: string;
  address: string;
  country: string;
}

const PersonalDetails: React.FunctionComponent<Props> = (props: Props) => {

  const { title, nationality, birthday, address, city, country } = props;

  const renderListItem = (className: string, data: string) => {
    return (
      <li className={className}>
        <a>{data}</a>
      </li>
    )
  }
   
  return (
    <div className="personal-container container-block">
      <h2 className="container-block-title">{title}</h2>
      <ul className="list-unstyled personal-list">
        {renderListItem('nationality', 'Nationality:  ' + nationality)}
        {renderListItem('birthday', 'Age: ' + getAge(birthday) + ' years / '+ formatBirthday(birthday))}
        {renderListItem('address', 'Address:')}
        {renderListItem('address', '  ' + address)}
        {renderListItem('address', '  ' + city)}
        {renderListItem('address', '  ' + country)}
      </ul>
    </div>
  );

}

export default PersonalDetails;
