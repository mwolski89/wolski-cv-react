import * as React from 'react';


interface Props {
  name: string,
  currentCompany: string,
  currentRole: string,
  imagePath: string
}

 const ProfileContainer: React.FunctionComponent<Props> = (props: Props) => {

  const { name, currentCompany, currentRole, imagePath } = props; 
  
  const renderProfilePicture = () => {
    if (imagePath) {
      return (<img id="avatar" className="profile" src={imagePath} alt="" style={{ maxWidth: 185 }} />);
    }
    return null;
  }

  
  return (
    <div className="profile-container">
      {renderProfilePicture()}
      <h1 className="name" style={{ fontSize: 25 }}>{ name }</h1>
      <h3 className="tagline"> {currentRole} </h3>
      <h4 className="tagline"> @{currentCompany} </h4>
    </div>
  );
  
}

export default ProfileContainer;