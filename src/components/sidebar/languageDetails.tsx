import * as React from 'react';


interface Props {
  list: [];
  title: string;
}

const LanguageDetails: React.FunctionComponent<Props> = (props: Props) => {
  const { list, title } = props;

  const renderListItem = (item: any, i: number) => {
    return (
      <li key={i}>
        {item.name}
        <span className="lang-desc">  ({item.level})</span>
      </li>
    );
  }
  
  return (
    <div className="languages-container container-block">
      <h2 className="container-block-title" key="lang_header">{ title}</h2>
      <ul className="list-unstyled interests-list" key="lang_list">
        {list.map((item, i) => {
          return renderListItem(item, i);
        })}
      </ul>
    </div>
  );

}

export default LanguageDetails;
