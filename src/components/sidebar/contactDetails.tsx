import * as React from 'react';

interface Props {
  phoneNumber: string;
  mail: string;
  title: string;
}

const ContactDetails:React.FunctionComponent<Props> = (props) => {

  const { title, phoneNumber, mail } = props;

  const renderListItem = (className: string, data: string, iconName: string, type: string) => {
    if (!data) { return null; }
    let href = data;
    switch (type) {
      case 'email':
        href = `mailto:${data}`;
        break;
      case 'phone':
        href = `tel:${data}`;
        break;
    }
    if (type !== undefined) {
      return (
        <li className={className}>
          <i className={`fa ${iconName}`} />
          <a href={`//${href}`} target="_blank"> {data} </a>
        </li>
      );
    } else {
      return (
        <li className={className}>
          <i className={`fa ${iconName}`} />
          <a> {data} </a>
        </li>
      )
    }
  }
  
  return (
    <div className="contact-container container-block">
      <h2 className="container-block-title">{title || 'Contact'}</h2>
      <ul className="list-unstyled contact-list">
        {renderListItem('phone', phoneNumber, 'fa-phone', 'phone')}
        {renderListItem('email', mail, 'fa-envelope', 'email')}
       </ul>
    </div>
  );
}

export default ContactDetails;