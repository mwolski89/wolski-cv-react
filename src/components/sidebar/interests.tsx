import * as React from 'react';

interface Props {
  list: [];
  title: string;
}
const LanguageDetails: React.FunctionComponent<Props> = (props: Props) => {

  const { list, title } = props;

  const renderListItem = (item: any) => {
    return (
      <li key={item}>
        {item}
      </li>
    );
  }
  
  return (
    <div className="languages-container container-block">
      <h2 className="container-block-title">{title}</h2>
      <ul className="list-unstyled interests-list">
        {list.map((item) => {
          return renderListItem(item);
        })}
      </ul>
    </div>
  );
  
}

export default LanguageDetails;
