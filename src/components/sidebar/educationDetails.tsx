import * as React from 'react';

interface Props {
  list: [];
  title: string;
}

const EducationDetails:React.FunctionComponent<Props> = (props) => {
  
  const renderListItem = (item: any, i: number) => {
    return (
      <div className="item" key={`education_item_${i}`}>
        <h4 className="degree">{item.degree}</h4>
        <h5 className="school">{item.school}</h5>
        <div className="time">{item.date}</div>
      </div>
    );
  }
  
  const { list, title } = props; 
  return (
      <div className="education-container container-block">
        <h2 className="container-block-title">{title}</h2>
        {list.map((item, i) => {
          return renderListItem(item, i);
        })}
      </div>
    );
}


export default EducationDetails;