import * as React from 'react';
import EducationDetails from './educationDetails';
import Interests from './interests';
import LanguageDetails from './languageDetails';
import ProfileContainer from './profileContainer';
import ContactDetails from './contactDetails';
import PersonalDetails from './personalDetails';
import SocialMedia from './socialMedia';

interface Props {
  interests: any;
  languages: any;
  educationDetails: any;
  contactDetails: any;
  personalDetails: any;
  socialMedia: any;
  name: string;
  currentRole: string;
  currentCompany: string;
  imagePath: string;
}

const Sidebar: React.FunctionComponent<Props> = (props) => {
  const { interests, languages, educationDetails, contactDetails, personalDetails, socialMedia, name, currentRole, currentCompany, imagePath } = props;
  
  const renderInterests = () => {
    if (interests) {
      return (<Interests list={interests.list} title={interests.sectionTitle} />);
    }
    return null;
  }
  
  const renderLanguages = () => {
    if (languages) {
      return (<LanguageDetails list={languages.list} title={languages.sectionTitle} />);
    }
    return null;
  }

  const renderEducationDetails = () => {
    if (educationDetails) {
      return (<EducationDetails list={educationDetails.list} title={educationDetails.sectionTitle} />);
    }
    return null;
  }

  const renderProfileContainer = () => {
    return (<ProfileContainer
      name={name}
      currentRole={currentRole}
      currentCompany={currentCompany}
      imagePath={imagePath}
    />);
  }

  const renderPersonalDetails = () => {
    if(personalDetails) {
      return (<PersonalDetails
        title={personalDetails.sectionTitle}
        birthday={personalDetails.birthday}
        nationality={personalDetails.nationality}
        city={personalDetails.city}
        address={personalDetails.address}
        country={personalDetails.country}
        website={personalDetails.website}
      />);
    } else { 
      return null 
    }
    
  }

  const renderSocialMedia = () => {
    if(socialMedia) {
      return (<SocialMedia
        title={socialMedia.sectionTitle}
        website={socialMedia.website}
        github={socialMedia.github}
        linkedin={socialMedia.linkedin}
        stackoverflow={socialMedia.stackoverflow}
      />)
    } else {
      return null;
    }
  }

  const renderContactDetails = () => {
    if(contactDetails) {
      return (<ContactDetails
        title={contactDetails.title}
        mail={contactDetails.mail}
        phoneNumber={contactDetails.phoneNumber}
      />);
    } else {
      return null;
    }
    
  }

    return (
      <div className="sidebar-wrapper">
        {renderProfileContainer()}
        {renderPersonalDetails()}
        {renderContactDetails()}
        {renderEducationDetails()}
        {renderLanguages()}
        {renderSocialMedia()}
        {renderInterests()}
      </div>
    );
}

// Sidebar.propTypes = {
//   name: string,
//   title: string,
//   imagePath: string,
//   mail: string,
//   phoneNumber: string,
//   city: string,
//   website: string,
//   linkedin: string,
//   github: string,
//   twitter: string,
//   educationDetails: PropTypes.shape().isRequired,
//   languages: PropTypes.shape().isRequired,
//   interests: PropTypes.shape().isRequired,
// };

// Sidebar.defaultProps = {
//   imagePath: null,
//   city: null,
//   website: null,
//   phoneNumber: null,
//   linkedin: null,
//   github: null,
//   twitter: null
// };

export default Sidebar;