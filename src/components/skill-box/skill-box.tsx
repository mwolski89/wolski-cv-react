import * as React from 'react';


interface Props {
  skill: any;
}

const SkillBox: React.FunctionComponent<Props> = (props: Props) => {
  
  const { skill } = props;

  return(
    <div className="skill-box">
      <span>{skill.name}</span>
      <span className="skill-level-value"> {skill.level}%</span>
      <div className="fillers">
        <div className="level-filler" style={{width: skill.level + '%'}}/>
        <div className="main-filler" />
      </div>
      <div/>
    </div>
  );
}

export default SkillBox;