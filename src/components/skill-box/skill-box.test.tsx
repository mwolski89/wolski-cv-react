import * as React from 'react';
import { render, screen } from '@testing-library/react'

import SkillBox from './skill-box';


it('should render SkillBox', () => {
    const knowledge = {name: 'iOS, Objective-C, Swift', level: 100};
    render(
        <SkillBox skill={knowledge} key={knowledge.name}/>
    );
    expect(screen.getByText(knowledge.name)).toBeInTheDocument();
    expect(screen.getByText(knowledge.level + "%")).toBeInTheDocument();
});