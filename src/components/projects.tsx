import * as React from 'react';
import { formatDateToString } from './../helpers/date-utils';
import Section from './shared/section';


interface Props {
  categories: [];
  description: string;
  sectionTitle: string;
  icon: string;
}

const Projects: React.FunctionComponent<Props> = (props: Props) => {
  
  const { icon, sectionTitle, categories, description } = props;

  const renderListItem = (item:any , i: number) => {
    return (
      <div className={`item ${item.shouldPageBreak ? "page-break": ""}`} key={`project_item_${i}`}>
        {renderProjectTitle(item)}
        -
        <span
          style={{ marginLeft: 5 }}
          className="project-tagline"
          dangerouslySetInnerHTML={{ __html: item.description }}
        />
        {renderProjectInformation(item)}
      </div>
    );
  }

  const renderProjectInformation = (project: any) => {
    const timeAtProject = `${formatDateToString(project.dateFrom)} - ${formatDateToString(project.dateTo)}`;
    return(
      <div className="project-information upper-row">
        <i className={`fa fa-user`} /> 
        <span>{project.role}</span>
        <div className="time">
          {timeAtProject}
        </div>
        <div>
          <i className="fa fas fa-tools" />
          {project.technologies.map((tech: any, idx: number) =>{
            return <span key={tech}>{tech}{idx < project.technologies.length - 1 ? ',' : ''} </span>
          })}
        </div>
      </div>
    );
  }
  const renderProjectTitle = (project: any) => {
    let inner = project.title;
    // if (project.url) {
    //   inner = <a href={project.url} target="_blank">{project.title}</a>;
    // }
    return (<span className="project-title" style={{ marginRight: 5 }}>{inner}</span>);
  }
  
  const renderIntro = () => {
    if (!description) { 
      return null; 
    }
    
    return (
      <div className="intro">
        <p dangerouslySetInnerHTML={{ __html: description }} />
      </div>);
  }

  const renderCategory = (category: any) => {
    return (
      <div className={`category-item ${category.shouldPageBreak ? "page-break" : ""}`} key={category.name + category.list.length}>
        <h5>{category.name}</h5>
        <span>{category.city}</span>
        <hr />
        {
          category.list.map((item: any, i:number) => renderListItem(item, i))
        }
      </div>
    );
  }
    return (
      <Section
        className="projects-section"
        icon={icon}
        id="projects"
        title={sectionTitle}
      >
        { renderIntro() }
        <div className="projects">
          {
            categories.map((c) => {
              return renderCategory(c);
            })
          }
        </div>
      </Section>
    );
}

export default Projects;