import * as React from 'react';

interface Props {
  id: string;
  icon: string;
  title: string;
  className: string;
  children: any;
}

const Section: React.FunctionComponent<Props> = (props) =>  {

  const { className, id, icon, title, children } = props;
    return (
      <section className={`section ${className || ''}`} id={id}>
        <h2 className="section-title">
          <i className={`fa ${icon}`} />
          { title }
        </h2>
        { children }
      </section>
    );
}

export default Section;