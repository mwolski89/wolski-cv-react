import * as React from 'react';
import { render, screen } from '@testing-library/react';

import Section from './section';

it("should render a section with children and title", () => {
    // GIVEN
    const className = "about-me-section";
    const icon = "";
    const sectionId = "sectionTestId";
    const sectionTitle = "section title test";
    const child = "This is some random child text";

    // WHEN
    render(
        <Section
            className={className}
            icon={icon}
            id={sectionId}
            title={sectionTitle}
        >
            {child}   
        </Section>
    );

    // THEN
    expect(screen.getByText(sectionTitle)).toBeInTheDocument();
    expect(screen.getByText(child)).toBeInTheDocument();
    expect(screen.getByDisplayValue)
    
});
